'use strict';

const Url = 'https://randomuser.me/api';

const users = async function (url,numUsers) {
    try {
        const results = [];
        const {results : users} = await (await fetch(`${url}?results=${numUsers}`)).json();
        results.push(...users);
        const userInfo = results.map(({login,name,gender,location,email,picture}) => {
            return {
                Usuario: login.username,
                Nombre: `${name.first} ${name.last}`,
                Género: gender,
                País: location.country,
                Email: email,
                Foto_Perfil: picture.medium

            };
        })
        console.log(userInfo);
    }

    catch (error) {
        console.error(error);
    }
}

const numUsers = prompt('Cuantos usuarios deseas?');
users(Url,numUsers);
