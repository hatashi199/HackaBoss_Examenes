'use strict';

const body = document.body;
const malla = document.querySelector('div.mallaCuadrados');
const addCuadrado = document.querySelector('div.botonCuadrado');

function random(max,min = 0) {
    return Math.floor((Math.random()*max + min) - min);
}

function cambioColor(newDiv) {
    setInterval(() => {
        newDiv.style.backgroundColor = `rgb(${random(255)},${random(255)},${random(255)})`;
    },1000)
}

const a = document.querySelector('div.botonCuadrado > a');
a.style.cssText = 
`
    text-decoration: none;
    color: #FFF;
    padding: 1rem 2rem;
    background-color: grey;
    font-weight: 700;
`

const frag = document.createDocumentFragment();

malla.style.cssText = 
    `
        display: flex;
        flex-wrap: wrap;
        width: 400px;
        padding: 2rem 0rem;
    `

for (let i = 0; i < random(12); i++) {
    const div = document.createElement('div');

    div.style.cssText = 
    `
        width: 100px;
        height: 100px;
    `;

    cambioColor(div);

    frag.append(div);
}

malla.append(frag);

const addCuadradoClick = (e) => {
    
    e.preventDefault();
    const {target} = e;

    if(target.matches('div.botonCuadrado > a')) {
        const div2 = document.createElement('div');
        div2.style.cssText = 
        `
            width: 100px;
            height: 100px;
        `;
        cambioColor(div2);
        malla.append(div2);
    }
}

addCuadrado.addEventListener('click', addCuadradoClick);




