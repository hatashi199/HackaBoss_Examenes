'use strict';

function binaryToDecimal(binary) {
    
    const arrayBinary = binary.toString().split('').reverse();

    let decimal = 0;

    for (let i = 0; i < arrayBinary.length; i++) {
        decimal += arrayBinary[i] * 2**i;
    }

    return decimal;
}

function decimalToBinary(decimal) {

    const counter = [];
    let dividendo = decimal;

    while (dividendo !== 1) {
        counter.push(dividendo % 2);
        dividendo = Math.floor(dividendo / 2);
    }

    counter.push(1);

    return counter.reverse().join('');

    //return Number(decimal.toString(2));

}

function numToBinOrDec(num,base) {
    if( base === 2 ) {
        return `Su binario es: ${decimalToBinary(num)}`;
    } else if(base === 10 ) {
        return `Su decimal es: ${binaryToDecimal(num)}`;
    } else {
        console.error('Número de base incorrecta');
    }
}

console.log(numToBinOrDec(10011,10));
console.log(numToBinOrDec(725,2));
