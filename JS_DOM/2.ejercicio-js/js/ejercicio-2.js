'use strict';

let h = 0;
let m = 0;
let s = 0;
let d = 0;

setInterval(() => {

    s += 5;

    if (s > 59){
        m++;
        s = 0;
    }else if (m > 59){
        h++;
        m = 0;
    }else if (h > 24){
        d++;
        h = 0;
    }

    let hour = h < 10 ? '0' + h : h;
    let sec = s < 10 ? '0' + s : s;
    let min = m < 10 ? '0' + m : m;
    let day = d < 10 ? '0' + d : d;

    console.log(`${day}:${hour}:${min}:${sec}`);
}, 5000);