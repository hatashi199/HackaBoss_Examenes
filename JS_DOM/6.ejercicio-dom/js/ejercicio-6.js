'use strict';

const body = document.body;
const p = document.querySelectorAll('p');

for (const parrafo of p) {

    let contenido = parrafo.innerHTML;

    const arrayContenido = contenido.split(/[\s.,"]+/);

    for (const palabra of arrayContenido) {
        if (palabra.length > 5) {
            contenido = contenido.split(`${palabra}`).join(`<span>${palabra}</span>`);
        }
    }
    parrafo.innerHTML = contenido;

    const subrayado = document.querySelectorAll('span');

    for (const span of subrayado) {
        span.style.cssText = `text-decoration: underline`;
    }
}