'use strict';

const Url = 'https://rickandmortyapi.com/api/';

const characters = async function (url) {
    try {
        const personajesEnero = [];

        const {episodes} = await (await fetch(url)).json();
        const {info} = await (await fetch(episodes)).json();

        let total_pages = info.pages;
        for (let i = 1; i <= total_pages; i++) {
            const {results} = await (await fetch(`${episodes}?page=${i}`)).json();

            const enero = results.filter((data) => {
                const mes = data.air_date;
                if (/January/.test(mes)) {
                    return data;
                }
            });

            const characters = enero.map(({characters}) => characters);

            console.log(characters);

            const arrayUrls = [];

            for (let j = 0; j < characters.length;j++) {

                arrayUrls.push(...characters[j]);

            }

            const filtroDuplicados = arrayUrls.filter((value,index) => {
                return arrayUrls.indexOf(value) === index;
            });

            for (let k = 0; k < filtroDuplicados.length; k++) {

                const {name} = await (await fetch(`${filtroDuplicados[k]}`)).json();
                personajesEnero.push(name);
            }

        }

        console.log(personajesEnero);
    }

    catch (error) {
        console.log(error);
    }

}

characters(Url);