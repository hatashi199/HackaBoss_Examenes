'use strict';

const h1 = document.querySelector('h1');

document.querySelector(".init").addEventListener("click", play);
document.querySelector(".pause").addEventListener("click",parar);
document.querySelector(".reset").addEventListener("click",reiniciar);

let h = 0;
let m = 0;
let s = 0;

let intervalo;

h1.innerHTML = "00:00:00";

function play() {
    intervalo = setInterval(escribir, 1000);

    document.querySelector(".init").removeEventListener("click", play);
}

function escribir() {
    s++;

    if (s > 59) {
        m++;
        s = 0;
    } else if (m > 59) {
        h++;
        m = 0;
    } else if (h > 24) {
        h = 0;
    }

    let hour = h < 10 ? '0' + h : h;
    let sec = s < 10 ? '0' + s : s;
    let min = m < 10 ? '0' + m : m;

    h1.innerHTML = `${hour}:${min}:${sec}`;
}

function parar() {
    clearInterval(intervalo);
    document.querySelector(".init").addEventListener("click", play);

}
function reiniciar(){
    clearInterval(intervalo);
    h1.innerHTML="00:00:00";

    h = 0;
    m = 0;
    s = 0;

    document.querySelector(".init").addEventListener("click",play);
}